sigrok-cli (0.7.2-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan McDowell <noodles@earth.li>  Fri, 24 Sep 2021 19:32:03 +0100

sigrok-cli (0.7.1-1) unstable; urgency=medium

  * New upstream release
  * Update appropriate bits to point at Salsa instead of Alioth

 -- Jonathan McDowell <noodles@earth.li>  Sat, 27 Oct 2018 18:23:40 +0100

sigrok-cli (0.7.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Jonathan McDowell <noodles@earth.li>  Mon, 18 Sep 2017 22:43:51 +0100

sigrok-cli (0.7.0-1) experimental; urgency=medium

  * New upstream release (Closes: #835484)
    + Updated build-dep versions: libglib2.0-dev (>= 2.32.0),
      libsigrok-dev (>= 0.5.0), libsigrokdecode-dev (>= 0.5.0).
  * Update copyright format URL to use https.
  * Move maintenance to Debian Electronics Packaging Team (Closes: #852834)
  * Standards-Version: 4.0.1
    + Change priority to optional
  * Bump debhelper compat level to 10

 -- Jonathan McDowell <noodles@earth.li>  Sat, 02 Sep 2017 08:12:47 +0100

sigrok-cli (0.5.0-1) unstable; urgency=medium

  * New upstream release.
    + Updated build-dep versions: libglib2.0-dev (>= 2.28.0),
      libsigrok-dev (>= 0.3.0), libsigrokdecode-dev (>= 0.3.0).
    + The non-existing --list-devices command-line option is no longer
      mentioned upstream (Closes: #737753).
  * Standards-Version: 3.9.5 (no changes required).
  * Switch from cdbs to debhelper.
  * debian/copyright: Update.

 -- Uwe Hermann <uwe@debian.org>  Sat, 24 May 2014 19:33:53 +0200

sigrok-cli (0.4.0-2) unstable; urgency=medium

  * Build-depend on the fixed libsigrok-dev (>= 0.2.0-2) and
    libsigrokdecode-dev (>= 0.2.0-2) packages (Closes: #721581).
    The libs have transitioned to unversioned -dev package names.
  * debian/copyright: Update.

 -- Uwe Hermann <uwe@debian.org>  Tue, 03 Sep 2013 9:09:20 +0200

sigrok-cli (0.4.0-1) unstable; urgency=low

  * New upstream release.
    + Build-depend on libsigrok1-dev, libsigrokdecode1-dev now.
  * Standards-Version: 3.9.4 (no changes required).
  * debian/watch: Update to new download URLs.

 -- Uwe Hermann <uwe@debian.org>  Sun, 02 Jun 2013 18:53:33 +0200

sigrok-cli (0.3.1-1) unstable; urgency=low

  * New upstream release.

 -- Uwe Hermann <uwe@debian.org>  Tue, 10 Jul 2012 23:06:27 +0200

sigrok-cli (0.3.0-2) unstable; urgency=low

  * Add missing build-deps on pkg-config, libglib2.0-dev (Closes: #674102).
  * Update build-deps on libsigrok/libsigrokdecode to >= 0.1.0-2.
  * Add 'Breaks: sigrok (<< 0.2-1)' and 'Replaces: sigrok (<< 0.2-1)'
    (this partially closes: #673854, the other parts will be in 'sigrok').

 -- Uwe Hermann <uwe@debian.org>  Wed, 23 May 2012 14:01:45 +0200

sigrok-cli (0.3.0-1) unstable; urgency=low

  * Initial release (Closes: #669072).

 -- Uwe Hermann <uwe@debian.org>  Sun, 13 May 2012 20:08:03 +0200
